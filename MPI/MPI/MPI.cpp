// MPI.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <mpi.h>
//#include <valarray>
#define RAND_MAX = 32767;

#pragma region Func

struct DecardCoord 
{
	double x;
	double y;
};

double VectorLength(DecardCoord v) 
{
	return sqrt(v.x*v.x + v.y*v.y);
}

int* GetArray(int size)
{
	int *a = new int[size];
	for (int i = 0; i < size; i++)
	{
		a[i] = rand()%100;
	}
	return a;
}

int* GetSignedArray(int size)
{
	int *a = new int[size];
	for (int i = 0; i < size; i++)
	{
		a[i] = rand() - 16384;
	}
	return a;
}

double* GetMArray(int size)
{
	double *a = new double[size];
	for (int i = 0; i < size; i++)
	{
		a[i] = rand();
	}
	return a;
}

int** GetMatrix(int s1, int s2)
{
	int **a = new int*[s1];
	for (int i = 0; i < s1; i++)
	{
		a[i] = GetArray(s2);/*new int[s2];
		for (int j = 0; j < s2; j++)
		{
			a[i][j] = rand();
		}*/
	}
	return a;
}

void PrintArr(int *ar, int size)
{
	printf("{");
	for (int i = 0; i < size; i++)
	{
		printf("%d ", ar[i]);
	}
	printf("}\n");
}

void PrintVect(int *ar, int size)
{
	//printf("{\n");
	for (int i = 0; i < size; i++)
	{
		printf("(%d)\n", ar[i]);
	}
	//printf("}\n");
}

void PrintMatrix(int **ar, int s1, int s2)
{
	printf("{\n");
	for (int i = 0; i < s1; i++)
	{
		PrintArr(ar[i], s2);
	}
	printf("}\n");
}

void PrintPlainMatrix(int *ar, int s1, int s2) 
{
	printf("{\n");
	for (int i = 0; i < s1; i++) 
	{
		printf("{");
		for (int j = 0; j < s2; j++) 
		{
			printf("%d ", ar[i*s2 + j]);
		}
		printf("}\n");
	}
	printf("}\n");
}

void PrintPlainDoubleMatrix(double *ar, int s1, int s2) 
{
	printf("{\n");
	for (int i = 0; i < s1; i++)
	{
		printf("{");
		for (int j = 0; j < s2; j++)
		{
			printf("%f ", ar[i*s2 + j]);
		}
		printf("}\n");
	}
	printf("}\n");
}

int Division(int * arr, int size, int *& left, int *& right, int * leftSize, int * rightSize, int op)
{
	int l = 0, r = 0;
	left = new int[size];
	right = new int[size];
	for (int i = 0; i < size; i++)
	{
		if (arr[i] > op)
		{
			right[r] = arr[i];
			r++;
		}
		else
		{
			left[l] = arr[i];
			l++;
		}
	}
	*leftSize = l;
	*rightSize = r;
	return 0;
}

int Fusion(int * arr1, int *arr2, int size1, int size2, int *& outarr) 
{
	outarr = arr1;
	for (int i = 0; i < size2; i++) 
	{
		outarr[i + size1] = arr2[i];
	}
	return 0;
}

void QuickSort(int *numbers, int left, int right)
{
	int pivot; // разрешающий элемент
	int l_hold = left; //левая граница
	int r_hold = right; // правая граница
	pivot = numbers[left];
	while (left < right) // пока границы не сомкнутся
	{
		while ((numbers[right] >= pivot) && (left < right))
			right--; // сдвигаем правую границу пока элемент [right] больше [pivot]
		if (left != right) // если границы не сомкнулись
		{
			numbers[left] = numbers[right]; // перемещаем элемент [right] на место разрешающего
			left++; // сдвигаем левую границу вправо 
		}
		while ((numbers[left] <= pivot) && (left < right))
			left++; // сдвигаем левую границу пока элемент [left] меньше [pivot]
		if (left != right) // если границы не сомкнулись
		{
			numbers[right] = numbers[left]; // перемещаем элемент [left] на место [right]
			right--; // сдвигаем правую границу вправо 
		}
	}
	numbers[left] = pivot; // ставим разрешающий элемент на место
	pivot = left;
	left = l_hold;
	right = r_hold;
	if (left < pivot) // Рекурсивно вызываем сортировку для левой и правой части массива
		QuickSort(numbers, left, pivot - 1);
	if (right > pivot)
		QuickSort(numbers, pivot + 1, right);
}

#pragma endregion

#pragma region BaseTasks

void Task1()
{
	int rank, num_thr;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	printf("threadId = %d, %d threads, HW!\n", rank, num_thr);
}

void Task2()
{
	int rank, num_thr;
	int n = 125;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *vect=new int[n];
	if (rank == 0)
	{
		vect = GetArray(n);
		PrintArr(vect, n);		
	}
	MPI_Bcast(vect, n, MPI_INT, 0, MPI_COMM_WORLD);
	int *min = new int[1];
	int *max = new int[1];
	min[0] = 32767;
	max[0] = 0;
	for (int i = rank; i < n; i = i + num_thr)
	{
		if (max[0] < vect[i])
		{
			max[0] = vect[i];
		}
		if (min[0] > vect[i])
		{
			min[0] = vect[i];
		}
	}
	int *minResult = new int[1];
	minResult[0] = 32767;
	int *maxResult = new int[1];
	maxResult[0] = 0;
	MPI_Reduce(max, maxResult, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(min, minResult, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
	if (rank == 0) 
	{
		printf("max = %d, min = %d", maxResult[0], minResult[0]);
	}
}

void Task3()
{
	int rank, num_thr;
	long n = 5000000;
	long *k = new long[1]; 
	k[0]= 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	double range = 2.0;
	srand(rank);
	for (long i = 0; i < n; i++) 
	{
		DecardCoord temp;
		temp.x = rand() / 16383.0 - 1.0;
		temp.y = rand() / 16383.0 - 1.0;
		if (VectorLength(temp) < 1.0 ) 
		{
			k[0]++;
		}
	}
	long *sum = new long[1];
	sum[0] = 0;
	MPI_Reduce(k, sum, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	if (rank == 0)
	{
		printf("%d\n", sum[0]);
		double pi = 4.0 * sum[0] / (n * num_thr);
		printf("pi = %f\n", pi);
	}
}

void Task4()
{
	int rank, num_thr;
	int n = 15; // it's expexted that n is 3 or more times larger than amount of threads. Otherwise there's no reason to use multithreading.
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *vect = new int[n];
	int *scat = new int[num_thr];
	int* displs = new int[num_thr];
	int portion = n / num_thr;
	bool unwhole_div = false;
	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}
	int* arr = new int[portion];
	if (rank == 0)
	{
		vect = GetSignedArray(n);
		PrintArr(vect, n);
		for (int i = 0; i < num_thr - 1; i++) 
		{
			scat[i] = portion;
			displs[i] = portion * i;
		}
		if (unwhole_div) 
		{
			scat[num_thr - 1] = n - num_thr * (portion-1);
		}
		else 
		{
			scat[num_thr - 1] = portion;
		}
		displs[num_thr - 1] = portion * (num_thr - 1);
	}
	MPI_Scatterv(vect, scat, displs, MPI_INT, arr, portion, MPI_INT, 0, MPI_COMM_WORLD);
	double *patrialAver = new double[2];
	double *averSum = new double[2];
	patrialAver[0] = 0.0;
	averSum[0] = 0.0;
	int k = 0; // amount of positive elements in the piece
	for (int i = 0; i < portion; i++) 
	{
		if (arr[i] > 0) 
		{
			patrialAver[0] += arr[i];
			k++;
		}
	}
	patrialAver[1] = k;
	MPI_Reduce(patrialAver, averSum, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (rank == 0) 
	{
		averSum[0] /= averSum[1];
		printf("average positive sum = %f\n", averSum[0]);
	}
}

void Task5() 
{
	int rank, num_thr;
	int n = 25;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *vect1 = new int[n];
	int *vect2 = new int[n];
	int *scat = new int[num_thr];
	int* displs = new int[num_thr];
	int portion = n / num_thr;
	bool unwhole_div = false;
	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}
	int* arr1 = new int[portion];
	int* arr2 = new int[portion];
	if (rank == 0)
	{
		vect1 = GetSignedArray(n);
		vect2 = GetSignedArray(n);
		PrintArr(vect1, n);
		PrintArr(vect2, n);
		for (int i = 0; i < num_thr - 1; i++)
		{
			scat[i] = portion;
			displs[i] = portion * i;
		}
		if (unwhole_div)
		{
			scat[num_thr - 1] = n - num_thr * (portion - 1);
		}
		else
		{
			scat[num_thr - 1] = portion;
		}
		displs[num_thr - 1] = portion * (num_thr - 1);
	}
	MPI_Scatterv(vect1, scat, displs, MPI_INT, arr1, portion, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatterv(vect2, scat, displs, MPI_INT, arr2, portion, MPI_INT, 0, MPI_COMM_WORLD);
	double *fracSum= new double[1];
	double *sum = new double[1];
	fracSum[0] = 0.0;
	sum[0] = 0.0;
	for (int i = 0; i < portion; i++)
	{
		fracSum[0] += (arr1[i] * arr2[i]);
	}
	MPI_Reduce(fracSum, sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (rank == 0)
	{
		printf("scalar multip = %f\n", sum[0]);
	}
}

void Task6()
{
	int rank, num_thr;
	const int n = 15;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	//int (*input_matrix)[n] = new int[n][n];
	//int *input_matrix[n];
	int* input_matrix = new int[n*n];
	int *scat = new int[num_thr];
	int *displs = new int[num_thr];
	int portion = n / num_thr;
	bool unwhole_div = false;
	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}

	int *frac_matrix = new int[portion*n];
	if (rank == 0)
	{
		input_matrix = GetArray(n*n);
		PrintPlainMatrix(input_matrix, n, n);
		for (int i = 0; i < num_thr - 1; i++)
		{
			scat[i] = portion * n;
			displs[i] = portion * i *n;
		}
		if (unwhole_div)
		{
			scat[num_thr - 1] = (n - num_thr * (portion - 1))*n;
		}
		else
		{
			scat[num_thr - 1] = portion*n;
		}
		displs[num_thr - 1] = portion * (num_thr - 1)*n;
	}
	MPI_Scatterv(input_matrix, scat, displs, MPI_INT, frac_matrix, portion*n, MPI_INT, 0, MPI_COMM_WORLD);
	int* stringMin = new int[2];
	int* threadMax = new int[1];
	int* threadMin = new int[1];
	int* globalMax = new int[1];
	int* globalMin = new int[1];
	threadMax[0] = 0; 
	threadMin[0] = 32767; // for minmax check
	globalMax[0] = 0;
	globalMin[0] = 32767; // for minmax check
	for (int i = 0; i < portion; i++)
	{
		stringMin[0] = 32767;
		stringMin[1] = 0; // for minmax check
		for (int j = 0; j < n; j++)
		{
			if (frac_matrix[i*n + j] < stringMin[0])
			{
				stringMin[0] = frac_matrix[i*n + j];
			}
			if (frac_matrix[i*n + j] > stringMin[1]) 
			{
				stringMin[1] = frac_matrix[i*n + j];
			}
		}
		if (stringMin[0] > threadMax[0]) 
		{
			threadMax[0] = stringMin[0];
		}
		if (stringMin[1] < threadMin[0]) 
		{
			threadMin[0] = stringMin[1];
		}
	}
	MPI_Reduce(threadMax, globalMax, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(threadMin, globalMin, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
	if (rank == 0)
	{
		printf("maxmin = %d, minmax = %d\n", globalMax[0], globalMin[0]);
	}
}

void Task7() //отображение матрицы транспонировано! (fixed)
{
	int rank, num_thr;
	const int n = 15;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int* input_matrix = new int[n*n];
	int* vect = new int[n];
	int *scat_m = new int[num_thr];
	int *displs_m = new int[num_thr];
	int *scat_v = new int[num_thr];
	int *displs_v = new int[num_thr];
	int portion = n / num_thr;
	bool unwhole_div = false;
	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}
	int *frac_matrix = new int[portion*n];
	int *frac_vect = new int[portion];
	if (rank == 0)
	{
		int ** input = GetMatrix(n, n);
		PrintMatrix(input, n, n);
		for (int i = 0; i < n; i++) // matrix transpositon
		{
			for (int j = 0; j < n; j++) 
			{
				input_matrix[i*n + j] = input[j][i];
			}
		}
		vect = GetArray(n);
		PrintVect(vect, n);
		for (int i = 0; i < num_thr - 1; i++)
		{
			scat_v[i] = portion;
			displs_v[i] = portion * i;
			scat_m[i] = portion * n;
			displs_m[i] = displs_v[i] *n;
		}
		if (unwhole_div)
		{
			scat_v[num_thr - 1] = (n - num_thr * (portion - 1));
			scat_m[num_thr - 1] = scat_v[num_thr - 1] * n;
		}
		else
		{
			scat_v[num_thr - 1] = portion;
			scat_m[num_thr - 1] = portion * n;
		}
		displs_v[num_thr - 1] = portion * (num_thr - 1);
		displs_m[num_thr - 1] = displs_v[num_thr - 1] *n;
	}
	MPI_Scatterv(input_matrix, scat_m, displs_m, MPI_INT, frac_matrix, portion*n, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatterv(vect, scat_v, displs_v, MPI_INT, frac_vect, portion, MPI_INT, 0, MPI_COMM_WORLD);
	double *localSum = new double[n];
	double *globalSum = new double[n];
	for (int j = 0; j < n; j++)
	{
		localSum[j] = 0;
		globalSum[j] = 0;
	}
	for (int i = 0; i < portion; i++) 
	{
		for (int j = 0; j < n; j++) 
		{
			localSum[j] += frac_matrix[i*n + j] * frac_vect[i];
		}
	}
	MPI_Reduce(localSum, globalSum, n, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (rank == 0) 
	{
		printf("{\n");
		for (int i = 0; i < n; i++) 
		{
			printf("%f\n", globalSum[i]);
		}
		printf("}\n");
	}
}

void Task8() 
{
	int rank, num_thr;
	int n = 16; 
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *input = new int[n];
	int *scat = new int[num_thr];
	int* displs = new int[num_thr];
	int portion = n / num_thr;
	int* arr = new int[portion];
	MPI_Status *status = new MPI_Status;
	if (rank == 0)
	{
		input = GetArray(n);
		PrintArr(input, n);
		for (int i = 0; i < num_thr; i++)
		{
			scat[i] = portion;
			displs[i] = portion * i;
		}
		for (int i = 1; i < num_thr; i++) 
		{
			MPI_Send(input + displs[i], scat[i], MPI_INT, i, i, MPI_COMM_WORLD);
		}
		for (int i = 0; i < portion; i++) 
		{
			arr[i] = input[i];
		}
	}
	else
	{		
		MPI_Recv(arr, portion, MPI_INT, 0, rank, MPI_COMM_WORLD, status);
	}
	printf("thread %d ", rank);
	PrintArr(arr, portion);
	if (rank != num_thr-1) 
	{
		MPI_Send(arr, portion, MPI_INT, num_thr - 1, 2 * (rank + 1), MPI_COMM_WORLD);
	}
	else
	{
		int *output = new int[n];
		for (int i = 0; i < num_thr - 1; i++) 
		{
			MPI_Recv(output + portion * i, portion, MPI_INT, i, 2 * (i + 1), MPI_COMM_WORLD, status);
		}
		for (int i = 0; i < portion; i++) 
		{
			output[portion*(num_thr - 1) + i] = arr[i];
		}
		printf("output (thr %d) ", rank);
		PrintArr(output, n);
	}
}

void Task9() 
{
	int rank, num_thr;
	int n = 15;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *vect = new int[n];
	int *scat = new int[num_thr];
	int* displs = new int[num_thr];
	int* gdispls = new int[num_thr];
	int portion = n / num_thr;
	bool unwhole_div = false;
	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}
	int* arr = new int[portion];
	int* output = new int[portion];
	if (rank == 0)
	{
		vect = GetSignedArray(n);
		PrintArr(vect, n);
		for (int i = 0; i < num_thr - 1; i++)
		{
			scat[i] = portion;
			displs[i] = portion * i;
			gdispls[i] = n - portion * (i+1);
		}
		if (unwhole_div)
		{
			scat[num_thr - 1] = n - num_thr * (portion - 1);
		}
		else
		{
			scat[num_thr - 1] = portion;
		}
		displs[num_thr - 1] = portion * (num_thr - 1);
		gdispls[num_thr - 1] = 0;
	}
	MPI_Scatterv(vect, scat, displs, MPI_INT, arr, portion, MPI_INT, 0, MPI_COMM_WORLD);
	for (int i = 0; i < portion; i++) 
	{
		output[i] = arr[portion - i - 1];
	}
	MPI_Gatherv(output, portion, MPI_INT, vect, scat, gdispls, MPI_INT, 0, MPI_COMM_WORLD);
	if (rank == 0) 
	{
		PrintArr(vect, n);
	}
}

void Task10() //I wonder why rsend sometimes crashes programm ("ready to send, but nowbody can recieve")
{
	int rank, num_thr;
	int n = 100000;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *arr1 = new int[n];
	int *arr2 = new int[n];
	int *buff = new int[2*n];
	MPI_Status *status = new MPI_Status();
	double time1, time2;
	MPI_Buffer_attach(buff, 2*sizeof(int)*n);
	if (rank == 0) 
	{
		arr1 = GetArray(n);

		time1 = MPI_Wtime();
		MPI_Send(arr1, n, MPI_INT, 1, 1, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Recv(arr1, n, MPI_INT, 1, 2, MPI_COMM_WORLD, status);
		MPI_Barrier(MPI_COMM_WORLD);
		time2 = MPI_Wtime() - time1;
		printf("MPI_Send = %f\n", time2);

		time1 = MPI_Wtime();
		MPI_Bsend(arr1, n, MPI_INT, 1, 3, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Recv(arr1, n, MPI_INT, 1, 4, MPI_COMM_WORLD, status);
		MPI_Barrier(MPI_COMM_WORLD);
		time2 = MPI_Wtime() - time1;
		printf("MPI_Bsend = %f\n", time2);
		

		time1 = MPI_Wtime();
		MPI_Ssend(arr1, n, MPI_INT, 1, 5, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Recv(arr1, n, MPI_INT, 1, 6, MPI_COMM_WORLD, status);
		MPI_Barrier(MPI_COMM_WORLD);
		time2 = MPI_Wtime() - time1;
		printf("MPI_Ssend = %f\n", time2);

		time1 = MPI_Wtime();
		MPI_Rsend(arr1, n, MPI_INT, 1, 7, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Recv(arr1, n, MPI_INT, 1, 8, MPI_COMM_WORLD, status);
		MPI_Barrier(MPI_COMM_WORLD);
		time2 = MPI_Wtime() - time1;
		printf("MPI_Rsend = %f\n", time2);

	}
	else
	{
		if (rank == 1)
		{
			MPI_Recv(arr2, n, MPI_INT, 0, 1, MPI_COMM_WORLD, status);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Send(arr2, n, MPI_INT, 0, 2, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);

			MPI_Recv(arr2, n, MPI_INT, 0, 3, MPI_COMM_WORLD, status);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Bsend(arr2, n, MPI_INT, 0, 4, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);

			MPI_Recv(arr2, n, MPI_INT, 0, 5, MPI_COMM_WORLD, status);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Ssend(arr2, n, MPI_INT, 0, 6, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);

			MPI_Recv(arr2, n, MPI_INT, 0, 7, MPI_COMM_WORLD, status);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Rsend(arr2, n, MPI_INT, 0, 8, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);

		}
		else
		{
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
}

void Task11() 
{
	int rank, num_thr;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *arr1 = new int[1];
	arr1[0] = 10;
	MPI_Status *status = new MPI_Status();
	if (rank == 0) 
	{
		printf("thread %d starts programm (n=%d)\n", rank, arr1[0]);
		arr1[0]++;
		MPI_Send(arr1, 1, MPI_INT, 1, 1, MPI_COMM_WORLD);
		MPI_Recv(arr1, 1, MPI_INT, num_thr-1, num_thr, MPI_COMM_WORLD, status);
		printf("thread %d ends programm (n=%d)\n", rank, arr1[0]);
	}
	else
	{
		for (int i = 1; i < num_thr; i++)
		{
			if (rank == i)
			{
				int k = (i + 1) % num_thr;
				MPI_Recv(arr1, 1, MPI_INT, i-1, i, MPI_COMM_WORLD, status);
				printf("thread %d is active (n=%d)\n",rank, arr1[0]);
				arr1[0]++;
				MPI_Send(arr1, 1, MPI_INT, k, i + 1, MPI_COMM_WORLD);
			}
		}
	}
}

#pragma endregion

void Task12()
{
	int rank, num_thr;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *arr1 = new int[1];
	arr1[0] = 10;
	MPI_Status *status = new MPI_Status();
	MPI_Comm comm1 = MPI_COMM_WORLD;
	MPI_Group x;
	MPI_Comm_group(MPI_COMM_WORLD, &x);
	for (int j = 0; j < 2; j++) {
		if (rank == 0)
		{
			printf("thread %d starts programm (n=%d) [%d]\n", rank, arr1[0], j);
			arr1[0]++;
			MPI_Send(arr1, 1, MPI_INT, 1, 1, comm1);
			MPI_Recv(arr1, 1, MPI_INT, num_thr - 1, num_thr, comm1, status);
			printf("thread %d ends programm (n=%d) [%d]\n", rank, arr1[0], j);
		}
		else
		{
			for (int i = 1; i < num_thr; i++)
			{
				if (rank == i)
				{
					int k = (i + 1) % num_thr;
					MPI_Recv(arr1, 1, MPI_INT, i - 1, i, comm1, status);
					printf("thread %d is active (n=%d) [%d]\n", rank, arr1[0], j);
					arr1[0]++;
					MPI_Send(arr1, 1, MPI_INT, k, i + 1, comm1);
				}
			}
		}
		MPI_Comm_create(MPI_COMM_WORLD, x, &comm1);
	}
	if (rank == 0)
	{
		int f;
		MPI_Comm_compare(MPI_COMM_WORLD, comm1, &f);
		printf("      equality:%d", f);
	}
}

void Task13() 
{
	const int n = 8;

#pragma region ParamInit
	int rank, num_thr;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int* input_matrix = new int[n*n];
	int *scat_m = new int[num_thr];
	int *displs_m = new int[num_thr];
	int portion = n / num_thr;
	int * transp = new int[n*n]; // передаточный буфер
	bool unwhole_div = false;
	int *frac_matrix = new int[portion*n];
	int activity = num_thr - 1;
	int sum = 0; // flag
	int i = 0;
	int j = 0;
	MPI_Request *requestsOut = new MPI_Request[n*n];
	MPI_Request *requestsIn = new MPI_Request[n*n];
	int requestOutId = 0;
	int requestInId = 0;
	int * incomeBuff = new int[3 * n * n*portion / 2];
	int * exitFlag = new int[1];
	exitFlag[0] = 0;
#pragma endregion

	if (n % num_thr != 0)
	{
		unwhole_div = true;
		if (rank == num_thr - 1)
		{
			portion = n - num_thr * portion;
		}
		else
		{
			portion++;
		}
	}
	if (rank == 0)
	{
		//int ** input = GetArray(n * n);
		//int * input = new int[n];

		//int inp1[16] =
		//{
		//	1,0,0,0,
		//	0,1,1,0,
		//	0,1,1,0,
		//	0,0,0,1
		//};

		int inp1[64] =
		{
			1,0,0,0,0,0,0,0,
			0,1,0,0,0,0,0,0,
			0,0,1,0,0,0,0,0,
			0,0,0,1,0,0,0,0,
			0,0,0,1,1,0,0,0,
			0,0,0,0,0,1,0,0,
			0,0,0,0,0,0,1,0,
			0,0,0,0,0,0,0,1
		};

		input_matrix = inp1;

		//PrintPlainMatrix(input_matrix, n, n);

		for (int i = 0; i < num_thr - 1; i++)
		{
			scat_m[i] = portion * n;
			displs_m[i] = portion * i * n;
		}
		if (unwhole_div)
		{
			scat_m[num_thr - 1] = (n - num_thr * (portion - 1)) * n;
		}
		else
		{
			scat_m[num_thr - 1] = portion * n;
		}
		displs_m[num_thr - 1] = portion * (num_thr - 1) * n;
	}

	MPI_Scatterv(input_matrix, scat_m, displs_m, MPI_INT, frac_matrix, portion*n, MPI_INT, 0, MPI_COMM_WORLD);

	i = 0;
	while ((i < portion) && (exitFlag[0] == 0))
	{
		j = 0;
		while ((j < n) && (exitFlag[0] == 0))
		{
			if (portion*rank + i != j)  // Элементы главной диагонали игнорируем
			{

				if (portion*rank + i < j) // чтобы дважды не сравнивать, заодно инициализируем асинхронный прием
				{								  // т.е. берем те элементы что выше главной диагонали

					if (j >= portion * (rank + 1)) // пересылаем элемент в другой процесс (неблокирующе)
					{
						int dest = j / portion; // номер процесса, куда отправляем
						transp[requestOutId * 3] = frac_matrix[i*n + j];
						transp[requestOutId * 3 + 1] = i + portion * rank;
						transp[requestOutId * 3 + 2] = j;
						MPI_Isend(transp + requestOutId * 3, 3, MPI_INT, dest, (j)*n + i + portion * rank, MPI_COMM_WORLD, requestsOut + requestOutId);
						requestOutId++;
					}
					else // элемент-партнер находится внутри памяти потока
					{

						if (frac_matrix[i*n + j] != frac_matrix[(j%portion)*n + portion * rank + i])
						{
							j = n;
							i = portion;
							sum = 1;
							exitFlag[0] = 1;
							MPI_Bcast(exitFlag, 1, MPI_INT, rank, MPI_COMM_WORLD); // Передаем флаг завершения работы
						}
					}
				}
				else
				{	//принимаем элемент из другого потока					
					if (j < portion * rank)
					{
						int source = j / portion;
						MPI_Irecv(incomeBuff + requestInId * 3, 3, MPI_INT, source, (i + portion * rank)*n + j, MPI_COMM_WORLD, requestsIn + requestInId);
						requestInId++;
					}
				}
			}
			j++;
		}
		i++;
	}

	MPI_Barrier(MPI_COMM_WORLD); // синхронизируем, чтобы буфера заполнились

	if (exitFlag[0] != 0) 
	{
		if(rank==0)
		{
			printf("matrix is not simmetric");
		}
	}
	else 
	{
		for (int k = 0; k < requestInId; k++)
		{
			int el = incomeBuff[k*3];
			i = incomeBuff[k*3 + 1];
			j = incomeBuff[k*3 + 2];
			int d = j % portion;
			if (el != frac_matrix[d*n + i])
			{
				sum = 1;
				break;
			}
		}
		i = 0;
		MPI_Reduce(&sum, &i, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
		if (rank == 0)
		{
			bool f = false;
			if (i == 0) 
			{
				f = true;
			}			
			printf("simmetric: ");
			std::cout << f;
		}
	}
}

void Task19() 
{
    int n = 16;
	int rank, num_thr;
	int *bufff = new int[n * n];
	MPI_Buffer_attach(bufff, sizeof(int)*n*n);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_thr);
	int *arr = new int[n];
	int *op = new int;
	if (rank == 0) 
	{
		arr= GetArray(n);
		*op = arr[n / 2];
		PrintArr(arr, n);
		//std::cout << op;
		printf("      %d\n", op[0]);
	}
	int fr_arrS = n / 4;
	int* fr_arr = new int[n];
	MPI_Scatter(arr, n / 4, MPI_INT, fr_arr, n / 4, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(op, 1, MPI_INT, 0, MPI_COMM_WORLD);
	//std::cout << *op;

	int leftS, rightS;
	int * leftP = new int[fr_arrS], *rightP=new int[fr_arrS];
	Division(fr_arr, fr_arrS, leftP, rightP, &leftS, &rightS, *op);

	int * buff = new int[n];
	int * rec = new int[n];
	int dest, tag;
	int * remain;
	int remainS;
	
	if (rank < num_thr / 2) 
	{
		buff = rightP;
		buff[n-1] = rightS;
		dest = rank + 2;
		tag = rank;
		remain = leftP;
		remainS = leftS;
	}
	else 
	{
		buff = leftP;
		buff[n-1] = leftS;
		dest = rank - 2;
		tag = rank - 2;
		remain = rightP;
		remainS = rightS;
	}
	MPI_Bsend(buff, n, MPI_INT, dest, tag, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Status st;
	MPI_Recv(rec, n, MPI_INT, dest, tag, MPI_COMM_WORLD, &st);
	fr_arrS = rec[n - 1] + remainS;
	Fusion(remain, rec, remainS, rec[n - 1], fr_arr);

	leftP = new int[fr_arrS];
	rightP = new int[fr_arrS];
	if (rank % 2 == 0) 
	{
		*op = fr_arr[0];
		//printf("thr %d and %d, op = %d\n", rank, rank + 1, op[0]);
		MPI_Bsend(op, 1, MPI_INT, rank + 1, rank + 100, MPI_COMM_WORLD);
		Division(fr_arr, fr_arrS, leftP, rightP, &leftS, &rightS, *op);
		buff = rightP;
		buff[n - 1] = rightS;
		dest = rank + 1;
		tag = rank;
		remain = leftP;
		remainS = leftS;
	}
	else 
	{
		MPI_Recv(op, 1, MPI_INT, rank - 1, rank + 99, MPI_COMM_WORLD, &st);
		Division(fr_arr, fr_arrS, leftP, rightP, &leftS, &rightS, *op);
		buff = leftP;
		buff[n - 1] = leftS;
		dest = rank - 1;
		tag = rank - 1;
		remain = rightP;
		remainS = rightS;
	}
	rec = new int[n];
	MPI_Bsend(buff, n, MPI_INT, dest, tag, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(rec, n, MPI_INT, dest, tag, MPI_COMM_WORLD, &st);
	fr_arrS = rec[n - 1] + remainS;
	Fusion(remain, rec, remainS, rec[n - 1], fr_arr);
	QuickSort(fr_arr, 0, fr_arrS - 1);
	if (rank != 0) 
	{
		MPI_Send(&fr_arrS, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
		//fr_arr[n - 1] = fr_arrS;
		MPI_Send(fr_arr, fr_arrS, MPI_INT, 0, rank + 100, MPI_COMM_WORLD);
	}
	else 
	{
		arr = new int[n];
		//arr = fr_arr;
		//int k = fr_arrS;
		int k = 0;
		for (int j = 0; j < fr_arrS; j++)
		{
			arr[k + j] = fr_arr[j];
		}
		k += fr_arrS;
		for (int i = 1; i < num_thr; i++) 
		{
			MPI_Recv(&fr_arrS, 1, MPI_INT, i, i, MPI_COMM_WORLD, &st);
			MPI_Recv(fr_arr, fr_arrS, MPI_INT, i, i + 100, MPI_COMM_WORLD, &st);
			//fr_arrS = fr_arr[n - 1];
			for (int j = 0; j < fr_arrS; j++) 
			{
				arr[k + j] = fr_arr[j];
			}
			k += fr_arrS;
		}
		PrintArr(arr, n);
	}
	printf("");  // хотите фокус? закомментьте эту строку
}

#pragma region Fox_Matrix_Multiplication

int ShowData = 0; // 1 - show, 0 - don't
int ProcNum = 0; // Number of available processes
int ProcRank = 0; // Rank of current process
int GridSize; // Size of virtual processor grid
int GridCoords[2]; // Coordinates of current processor in grid
int RealSize;
MPI_Comm GridComm; // Grid communicator
MPI_Comm ColComm; // Column communicator
MPI_Comm RowComm; // Row communicator

void PrintBalancedPlainDoubleMatrix(double *ar, int fullSize, int realSize)
{
	printf("{\n");
	for (int i = 0; i < realSize; i++)
	{
		printf("{");
		for (int j = 0; j < realSize; j++)
		{
			printf("%f ", ar[i*fullSize + j]);
		}
		printf("}\n");
	}
	printf("}\n");
}

double* GetBalancedDoubleMatrix(int fullSize, int realSize)
{
	double *a = new double[fullSize*fullSize];
	for (int i = 0; i < fullSize; i++)
	{
		for (int j = 0; j < fullSize; j++)
		{
			if ((i < realSize) && (j < realSize))
			{
				a[i*fullSize + j] = rand();
			}
			else
			{
				a[i*fullSize + j] = 0;
			}
		}
	}
	return a;
}


// Creation of two-dimensional grid communicator
// and communicators for each row and each column of the grid
void CreateGridCommunicators() {
	int DimSize[2]; // Number of processes in each dimension of the grid
	int Periodic[2]; // =1, if the grid dimension should be periodic
	int Subdims[2]; // =1, if the grid dimension should be fixed
	DimSize[0] = GridSize;
	DimSize[1] = GridSize;
	Periodic[0] = 0;
	Periodic[1] = 0;
	// Creation of the Cartesian communicator
	MPI_Cart_create(MPI_COMM_WORLD, 2, DimSize, Periodic, 1, &GridComm);
	// Determination of the cartesian coordinates for every process
	MPI_Cart_coords(GridComm, ProcRank, 2, GridCoords);
	// Creating communicators for rows
	Subdims[0] = 0; // Dimensionality fixing
	Subdims[1] = 1; // The presence of the given dimension in the subgrid
	MPI_Cart_sub(GridComm, Subdims, &RowComm);
	// Creating communicators for columns
	Subdims[0] = 1;
	Subdims[1] = 0;
	MPI_Cart_sub(GridComm, Subdims, &ColComm);
}

int RandomDataInitialization(double *& pAMatrix, double *& pBMatrix, int * Size)
{
	pAMatrix = GetBalancedDoubleMatrix(*Size, RealSize);
	pBMatrix = GetBalancedDoubleMatrix(*Size, RealSize);
	if (ShowData == 1) 
	{
		std::cout << "a=\n";
		PrintBalancedPlainDoubleMatrix(pAMatrix, *Size, RealSize);
		std::cout << "b=\n";
		PrintBalancedPlainDoubleMatrix(pBMatrix, *Size, RealSize);
		//std::cout << "a full=\n";
		//PrintPlainDoubleMatrix(pAMatrix, *Size, *Size);
	}
	return 0;
}

// Function for memory allocation and data initialization
void ProcessInitialization(double* &pAMatrix, double* &pBMatrix,
	double* &pCMatrix, double* &pAblock, double* &pBblock, double* &pCblock,
	double* &pTemporaryAblock, int &Size, int &BlockSize) 
{
	if (ProcRank == 0) 
	{
		printf("\nEnter matrixs size: ");
		scanf("%d", &Size);
		RealSize = Size;
		if (Size%GridSize != 0) 
		{
			Size = (Size / GridSize + 1)*GridSize;
		}
	}

	MPI_Bcast(&Size, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&RealSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
	BlockSize = Size / GridSize;
	pAblock = new double[BlockSize*BlockSize];
	pBblock = new double[BlockSize*BlockSize];
	pCblock = new double[BlockSize*BlockSize];
	pTemporaryAblock = new double[BlockSize*BlockSize];
	for (int i = 0; i < BlockSize*BlockSize; i++) 
	{
		pCblock[i] = 0;
	}
	if (ProcRank == 0) 
	{
		pAMatrix = new double[Size*Size];
		pBMatrix = new double[Size*Size];
		pCMatrix = new double[Size*Size];
		RandomDataInitialization(pAMatrix, pBMatrix, &Size);
	}
}

void DataDistribution(double * pAMatrix, double * pBMatrix, double *& pMatrixAblock, double *& pBblock, int Size, int BlockSize)
{
	int sentSize = BlockSize * BlockSize * GridSize;
	double * ABuff = new double[sentSize];
	double * BBuff = new double[sentSize];
	// рассылаем по 0 столбцу, каждый из столбца разошлет на свою строку.
	if (GridCoords[1] == 0) 
	{
		MPI_Scatter(pAMatrix, sentSize, MPI_DOUBLE, ABuff, sentSize, MPI_DOUBLE, 0, ColComm);
		MPI_Scatter(pBMatrix, sentSize, MPI_DOUBLE, BBuff, sentSize, MPI_DOUBLE, 0, ColComm);
	}
	sentSize =BlockSize;

	for (int i = 0; i < BlockSize; i++)
	{
		MPI_Scatter(ABuff + i * Size, sentSize, MPI_DOUBLE, pMatrixAblock + i * BlockSize, sentSize, MPI_DOUBLE, 0, RowComm);
		MPI_Scatter(BBuff + i * Size, sentSize, MPI_DOUBLE, pBblock + i * BlockSize, sentSize, MPI_DOUBLE, 0, RowComm);
	}
}

void ResultCollection(double *& pCMatrix, double * pCblock, int Size, int BlockSize)
{
	int sentSize = BlockSize;
	double * CBuff = new double[BlockSize * BlockSize * GridSize];

	for (int i = 0; i < BlockSize; i++)
	{
		MPI_Gather(pCblock + i * BlockSize, sentSize, MPI_DOUBLE, CBuff + i * Size, sentSize, MPI_DOUBLE, 0, RowComm);
	}

	sentSize = BlockSize * BlockSize * GridSize;
	if (GridCoords[1] == 0) 
	{
		MPI_Gather(CBuff, sentSize, MPI_DOUBLE, pCMatrix, sentSize, MPI_DOUBLE, 0, ColComm);
	}

}

// Broadcasting matrix A blocks to process grid rows
void ABlockCommunication(int iter, double *pAblock, double* pMatrixAblock, int BlockSize) 
{
	// Defining the leading process of the process grid row
	int Pivot = (GridCoords[0] + iter) % GridSize;
	// Copying the transmitted block in a separate memory buffer
	if (GridCoords[1] == Pivot) 
	{
		for (int i = 0; i < BlockSize*BlockSize; i++)
			pAblock[i] = pMatrixAblock[i];
	}
	// Block broadcasting
	MPI_Bcast(pAblock, BlockSize*BlockSize, MPI_DOUBLE, Pivot, RowComm);
}

// Умножение матричных блоков
void BlockMultiplication(double *pAblock, double *pBblock, double *pCblock, int BlockSize) 
{
	for (int i = 0; i < BlockSize; i++) 
	{
		for (int j = 0; j < BlockSize; j++) 
		{
			double temp = 0;
			for (int k = 0; k < BlockSize; k++)
				temp += pAblock[i*BlockSize + k] * pBblock[k*BlockSize + j];
			pCblock[i*BlockSize + j] += temp;
		}
	}
}

// Cyclic shift of matrix B blocks in the process grid columns
void BblockCommunication(double *pBblock, int BlockSize) 
{
	MPI_Status Status;
	int NextProc = GridCoords[0] + 1;
	if (GridCoords[0] == GridSize - 1) NextProc = 0;
	int PrevProc = GridCoords[0] - 1;
	if (GridCoords[0] == 0) PrevProc = GridSize - 1;
	MPI_Sendrecv_replace(pBblock, BlockSize*BlockSize, MPI_DOUBLE,
		NextProc, 0, PrevProc, 0, ColComm, &Status);
}

void ParallelResultCalculation(double* pAblock, double* pMatrixAblock,
	double* pBblock, double* pCblock, int BlockSize) 
{
	for (int iter = 0; iter < GridSize; iter++) 
	{
		// Sending blocks of matrix A to the process grid rows
		ABlockCommunication(iter, pAblock, pMatrixAblock, BlockSize);
		// Block multiplication
		BlockMultiplication(pAblock, pBblock, pCblock, BlockSize);
		// Cyclic shift of blocks of matrix B in process grid columns
		BblockCommunication(pBblock, BlockSize);
	}
}

void SingleThreadMultip(double * MatrixA, double * MatrixB, double *& MatrixC, int RealSize)
{
	int i, j, k;
	for (i = 0; i < RealSize; i++)
	{
		for (j = 0; j < RealSize; j++)
		{
			MatrixC[i*RealSize + j] = 0;
			for (k = 0; k < RealSize; k++)
			{
				MatrixC[i*RealSize + j] = MatrixC[i*RealSize + j] + MatrixA[i*RealSize + k] * MatrixB[k*RealSize + j];
			}
		}
	}
}

void Task16() 
{
	double* pAMatrix; // The first argument of matrix multiplication
	double* pBMatrix; // The second argument of matrix multiplication
	double* pCMatrix; // The result matrix
	int Size; // Size of matricies
	int BlockSize; // Sizes of matrix blocks on current process
	double *pAblock; // Initial block of matrix A on current process
	double *pBblock; // Initial block of matrix B on current process
	double *pCblock; // Block of result matrix C on current process
	double *pMatrixAblock;
	double Start, Finish, DurationMult, DurationSingle;
	setvbuf(stdout, 0, _IONBF, 0);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	GridSize = sqrt((double)ProcNum);
	if (ProcNum != GridSize * GridSize) 
	{
		if (ProcRank == 0) 
		{
			printf("Number of processes must be a perfect square \n");
		}
	}
	else 
	{
		if (ProcRank == 0) 
		{
			printf("Parallel matrix multiplication program\n");
		}
		// Creating the cartesian grid, row and column communcators
		CreateGridCommunicators();
		// Memory allocation and initialization of matrix elements
		ProcessInitialization(pAMatrix, pBMatrix, pCMatrix, pAblock, pBblock, pCblock, pMatrixAblock, Size, BlockSize);

		MPI_Barrier(MPI_COMM_WORLD);
		if (ProcRank == 0) 
		{
			Start = MPI_Wtime();
		}

		DataDistribution(pAMatrix, pBMatrix, pMatrixAblock, pBblock, Size, BlockSize);
		// Execution of Fox method
		ParallelResultCalculation(pAblock, pMatrixAblock, pBblock,

			pCblock, BlockSize);
		ResultCollection(pCMatrix, pCblock, Size, BlockSize);
		
		MPI_Barrier(MPI_COMM_WORLD);
		if (ProcRank == 0)
		{
			Finish = MPI_Wtime();
			DurationMult = Finish - Start;
			if (ShowData == 1) 
			{
				std::cout << "c=\n";
				PrintBalancedPlainDoubleMatrix(pCMatrix, Size, RealSize);
			}
		}

		MPI_Barrier(MPI_COMM_WORLD);
		if (ProcRank == 0)
		{
			Start = MPI_Wtime();
			SingleThreadMultip(pAMatrix, pBMatrix, pCMatrix, RealSize);
			Finish = MPI_Wtime();
			DurationSingle = Finish - Start;
			printf("matrix size = %d\nsingle-therad time = %f\n%d-threads time=%f\nboost=%f\n", RealSize, DurationSingle, ProcNum, DurationMult, DurationSingle / DurationMult);
		}
	}
}

void TestEnv() 
{
	double* pAMatrix; // The first argument of matrix multiplication
	double* pBMatrix; // The second argument of matrix multiplication
	double* pCMatrix; // The result matrix
	int Size; // Size of matricies
	int BlockSize; // Sizes of matrix blocks on current process
	double *pAblock; // Initial block of matrix A on current process
	double *pBblock; // Initial block of matrix B on current process
	double *pCblock; // Block of result matrix C on current process
	double *pMatrixAblock;
	//double Start, Finish, Duration;
	setvbuf(stdout, 0, _IONBF, 0);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
	GridSize = sqrt((double)ProcNum);
	if (ProcNum != GridSize * GridSize) 
	{
		if (ProcRank == 0) 
		{
			printf("Number of processes must be a perfect square \n");
		}
	}
	else {
		if (ProcRank == 0)
			printf("Parallel matrix multiplication program\n");
		// Creating the cartesian grid, row and column communcators
		CreateGridCommunicators();
		// Memory allocation and initialization of matrix elements
		ProcessInitialization(pAMatrix, pBMatrix, pCMatrix, pAblock, pBblock,
			pCblock, pMatrixAblock, Size, BlockSize);
		DataDistribution(pAMatrix, pBMatrix, pMatrixAblock, pBblock, Size, BlockSize);
		pCblock = pMatrixAblock;
		ResultCollection(pCMatrix, pCblock, Size, BlockSize);
		
	}
	//if (ProcRank == 0) 
	//{
	//	printf("%d thr - gr(%d,%d)\n", ProcRank, GridCoords[0], GridCoords[1]);
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
	//if (ProcRank == 1)
	//{
	//	printf("%d thr - gr(%d,%d)\n", ProcRank, GridCoords[0], GridCoords[1]);
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
	//if (ProcRank == 2)
	//{
	//	printf("%d thr - gr(%d,%d)\n", ProcRank, GridCoords[0], GridCoords[1]);
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
	//if (ProcRank == 3)
	//{
	//	printf("%d thr - gr(%d,%d)\n", ProcRank, GridCoords[0], GridCoords[1]);
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
}
#pragma endregion

int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);
	Task10();
	MPI_Finalize();
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.