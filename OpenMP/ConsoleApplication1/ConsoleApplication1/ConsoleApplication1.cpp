// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <omp.h>
#include <Windows.h>
#include <ctime>
#include <stack>
#define RAND_MAX = 32767;

omp_lock_t my_lock;
using namespace std;

struct ThreadRecord 
{
	int thr_num;
	int num_thr;
};

void ThreadReport() 
{
	int id = omp_get_thread_num();
	int numb = omp_get_num_threads();
	printf("threadId = %d, %d threads, HW!\n", id, numb);
}

int* GetArray(int size)
{
	int *a = new int[size];
	for (int i = 0; i < size; i++) 
	{
		a[i] = rand();
	}
	return a;
}

double* GetMArray(int size) 
{
	double *a = new double[size];
	for (int i = 0; i < size; i++)
	{
		a[i] = rand();
	}
	return a;
}

int** GetMatrix(int s1, int s2) 
{
	int **a = new int*[s1];
	for (int i = 0; i < s1; i++) 
	{
		a[i] = GetArray(s2);/*new int[s2];
		for (int j = 0; j < s2; j++) 
		{
			a[i][j] = rand();
		}*/
	}
	return a;
}

void PrintArr(int *ar, int size) 
{
	printf("{");
	for (int i = 0; i < size; i++)
	{
		printf("%d ", ar[i]);
	}
	printf("}\n");
}

void PrintMatrix(int **ar, int s1, int s2) 
{
	printf("{\n");
	for (int i = 0; i < s1; i++) 
	{
		PrintArr(ar[i], s2);
	}
	printf("}\n");
}

long MulMod(long x, long y, long m)
{
	return (x*y) % m;
}

long PowMod(long x, long a, long m)
{
	long r = 1;
	while (a > 0)
	{
		if (a % 2 != 0)
			r = MulMod(r, x, m);
		a = a >> 1;
		x = MulMod(x, x, m);
	}
	return r;
}

bool IsPrime(long p)
{
	long q, a;
	int i, rounds = log2(p);
	//cl_randex rand;
	//rand.set_max("100000000000"); // макс число для генератора сл. чисел
	srand(time(NULL));
	//if (p % 2 != 0 && p > 1)
	if ((p & 1) != 0 && p > 1)
	{
		q = p - 1;
		do q = q >> 1; while (q % 2 == 0);
		for (i = 1; i < rounds; i++)//i<=rounds
		{
			a = rand() % (p - 1) + 2; //rand.lgen - генератор сл. чисел
			if (PowMod(a, p - 1, p) != 1)
			{
				return false;
				//break;//break не нужен
			}
			a = PowMod(a, q, p);
			if (a != 1)
			{
				while (a != 1 && a != p - 1) a = MulMod(a, a, p);
				if (a == 1)
				{
					return false;
					//break;//break не нужен
				}
			}
		}
		return true;
	}
	else
		return false;//return t==2
}

void Task1() 
{
#pragma omp parallel num_threads(8)
	{
		ThreadReport();
	}
}

void Task2() 
{
	omp_set_num_threads(3);
#pragma omp parallel if(omp_get_max_threads()>1)
	{
		if (omp_get_num_threads() > 1) 
		{
			ThreadReport();
		}
	}
	omp_set_num_threads(1);
#pragma omp parallel if(omp_get_max_threads()>1)
	{
		if (omp_get_num_threads() > 1)
		{
			ThreadReport();
		}
	}
}

void Task3() 
{
	int a = 3, b = 10;
	printf("begin: a = %d, b = %d\n", a, b);
#pragma omp parallel num_threads(2) private(a) firstprivate(b)
	{
		int id = omp_get_thread_num();
		a = 0;
		a += id;
		b += id;
		printf("inside first parallel section(tr %d): a = %d, b = %d\n", id, a, b);
	}
	printf("after first ps: a = %d, b = %d\n", a, b);
#pragma omp parallel num_threads(4) shared(a) firstprivate(b)
	{
		int id = omp_get_thread_num();
		a -= id;
		b -= id;
		printf("inside second ps(tr %d): a = %d, b = %d\n", id, a, b);
	}
	printf("after second ps: a = %d, b = %d\n", a, b);
}

void Task4()
{
	int *a = GetArray(10);
	int *b = GetArray(10);
	printf("a = ");
	PrintArr(a, 10);
	printf("b = ");
	PrintArr(b, 10);
	int max = 0, min = 32767;
#pragma omp parallel shared(max,min) num_threads(2)
	{
	#pragma omp single 
		{
			for (int i = 0; i < 10; i++) 
			{
				if (a[i] < min) 
				{
					min = a[i];
				}
			}
			printf("min(a) = %d, work is done by %d thread\n", min, omp_get_thread_num());
		}
	#pragma omp single
		{
			for (int i = 0; i < 10; i++)
			{
				if (b[i] > max) 
				{
					max = b[i];
				}
			}
			printf("max(b) = %d, work is done by %d thread\n", max, omp_get_thread_num());
		}
	}
}

void Task5() 
{
	int **a = GetMatrix(6, 8);
	printf("a = ");
	PrintMatrix(a, 6, 8);
	double aver = 0; 
	int min = 32767, max = 0, k = 0;
#pragma omp parallel sections
	{
	#pragma omp section
		{
			for (int i = 0; i < 6; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					aver += a[i][j];
				}
			}
			aver /= 48;
			printf("average = %f, report is done by %d thread\n", aver, omp_get_thread_num());
		}
	#pragma omp section
		{
			for (int i = 0; i < 6; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					if (a[i][j] < min)
					{
						min = a[i][j];
					}
					if (a[i][j] > max)
					{
						max = a[i][j];
					}
				}
			}
			printf("min = %d, max= %d , report is done by %d thread\n", min, max, omp_get_thread_num());
		}
	#pragma omp section
		{
			for (int i = 0; i < 6; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					if (a[i][j] % 3 == 0)
					{
						k++;
					}
				}
			}
			printf("kr = %d , report is done by %d thread\n", k, omp_get_thread_num());
		}
	}
}

void Task6() 
{
	int *a = GetArray(10);
	int *b = GetArray(10);
	printf("a = ");
	PrintArr(a, 10);
	printf("b = ");
	PrintArr(b, 10);
	double aver_a = 0, aver_b = 0;
#pragma omp parallel for reduction(+:aver_a,aver_b)
		for (int i = 0; i < 10; i++) 
		{
			aver_a += a[i];
			aver_b += b[i];
		}
	aver_a /= 10;
	aver_b /= 10;
	char temp = 'a';
	if (aver_b > aver_a) 
	{
		temp = 'b';
	}
	printf("aver_a = %f, aver_b= %f, aver_%c is bigger\n", aver_a, aver_b, temp);
}

void Task7() 
{
	int n = 12;
	int *a = new int[n];
	int *b = new int[n];
	int *c = new int[n];
	//printf("a = ");
	//PrintArr(a, n);
	//printf("b = ");
	//PrintArr(b, n);
	//printf("c = ");
	//PrintArr(c, n);
	omp_set_num_threads(3);
#pragma omp parallel for shared(a,b) schedule(static,3)
	for (int i = 0; i < n; i++) 
	{
		a[i] = rand();
		b[i] = rand();
		printf("a[%d] = %d, b[%d] = %d, th_num = %d\n", i, a[i], i, b[i], omp_get_thread_num());
	}
	printf("\n");
	omp_set_num_threads(4);
#pragma omp parallel for shared(a,b,c) schedule(dynamic,3)
	for (int i = 0; i < n; i++)
	{
		c[i] = a[i] + b[i];
		printf("c[%d] = %d, th_num = %d\n", i, c[i], omp_get_thread_num());
	}
}

void Task8() 
{
	int n = 10000;
	int** m = GetMatrix(n, n);
	int* v = GetArray(n);
	double* r = new double[n];
	double sStart, pStart, pEnd;
	sStart = omp_get_wtime();
	for (int i = 0; i < n; i++)
	{
		r[i] = 0;
		for (int j = 0; j < n; j++)
		{
			r[i] += m[i][j] * v[j];
		}
		//printf("r[%d] = %f\n", i, r[i]);
	}
	pStart = omp_get_wtime();
#pragma omp parallel for shared(r)
	for (int i = 0; i < n; i++)
	{
		r[i] = 0;
		for (int j = 0; j < n; j++)
		{
			r[i] += m[i][j] * v[j];
		}
		//printf("r[%d] = %f , report is done by %d thread(%d)\n", i, r[i], omp_get_thread_num(), omp_get_num_threads());
	}
	pEnd = omp_get_wtime();
	printf("sigle thread = %f sec, parallel = %f sec", pStart - sStart, pEnd - pStart);
}

void Task9() 
{
	int m = 6, n = 8;
	int** d = GetMatrix(m, n);
	printf("d = ");
	PrintMatrix(d, m, n);
	int min = 32767, max = 0;
	omp_set_num_threads(3);
#pragma omp parallel for shared(min,max)
	for (int i = 0; i < m; i++) 
	{
		for (int j = 0; j < n; j++) 
		{
	#pragma omp critical //fsect
			{
				if (d[i][j] < min)
				{
					min = d[i][j];
				}
			}
	#pragma omp critical //ssect
			{
				if (d[i][j] > max)
				{
					max = d[i][j];
				}
			}
		}
	}
	printf("max = %d, min = %d\n", max, min);
}

void Task10() 
{
	int n = 30;
	int* a = GetArray(n);
	printf("a = ");
	PrintArr(a, n);
	int k = 0;
	omp_set_num_threads(3);
#pragma omp parallel for shared(k)
	for(int i=0;i<n;i++)
	{
		if (a[i] % 9 == 0) 
		{
		#pragma omp atomic
			k++;
		}
	}
	printf("k=%d\n", k);
}

void Task11() 
{
	int n = 100;
	int* a = GetArray(n);
	printf("a = ");
	PrintArr(a, n);
	int max = 0;
	omp_set_num_threads(4);
#pragma omp parallel for shared(max)
	for (int i = 0; i < n; i++) 
	{
		if (a[i] % 7 == 0)
		{
		#pragma omp critical
			{
				if (a[i] > max) {
					max = a[i];
				}
			}
		}
	}
	printf("max = %d\n", max);
}

void Task12() 
{
	omp_set_num_threads(8);
#pragma omp parallel 
	{
		int id = omp_get_thread_num();
		int numb = omp_get_num_threads();
		Sleep(10*(7 - id) + 1);
		printf("threadId = %d, %d threads, HW!\n", id, numb);
	}
	printf("\n");

	int k = 7;
#pragma omp parallel shared(k) //reduction(-:k)
	{
		int id = omp_get_thread_num();
		int numb = omp_get_num_threads();
		bool f = true;
		while (f) 
		{
		#pragma omp critical
			{
				if (id == k) 
				{
					printf("threadId = %d, %d threads, HW!\n", id, numb);
					f = false;
					k--;
				}
			}
			Sleep(10);
		}
	}
	printf("\n");

	omp_init_lock(&my_lock);
	k = 7;
#pragma omp parallel shared(k)
	{
		int id = omp_get_thread_num();
		int numb = omp_get_num_threads();
		bool f = true;
		while (f)
		{
			omp_set_lock(&my_lock);
			if (id == k)
			{
				printf("threadId = %d, %d threads, HW!\n", id, numb);
				f = false;
				k--;
			}
			omp_unset_lock(&my_lock);
			Sleep(10);
		}
	}
	printf("\n");

	srand(time(0));
#pragma omp parallel
	{
		int id = omp_get_thread_num();
		int numb = omp_get_num_threads();
		bool f = true;
		while (f)
		{
			double t1 = rand() / 32767.0;
			double t2 = 0.01 + id / 7.1;
			f = (t1 > t2);
			Sleep(50 * (8 - id));
		} 
		printf("threadId = %d, %d threads, HW!\n", id, numb);
	}
	printf("\n");

	stack <ThreadRecord> st;
	#pragma omp parallel 
	{
		ThreadRecord tempr;
		tempr.thr_num = omp_get_thread_num();
		tempr.num_thr = omp_get_num_threads();
		#pragma omp critical
		{
			st.push(tempr);
		}
	}
	while (st.empty()==false)
	{
		ThreadRecord tempr = st.top();
		printf("threadId = %d, %d threads, HW!\n", tempr.thr_num, tempr.num_thr);
		st.pop();
	}
	printf("\n");

}

void Task13()
{
	long n = 30;
	double sum = 0;
	//long a[6] = { 1,0,1,1,0,1 };
	long a[30] = { 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1 };
#pragma omp parallel for reduction(+:sum)
	for (int i = 0; i < n; i++) 
	{
		sum += a[i] << i;
	}
	printf("sum = %f\n", sum);
}

void Task14() 
{
	int sum = 0, n = 210;
#pragma omp parallel for reduction(+:sum)
	for (int i = 0; i < n; i++) 
	{
		sum += (i << 1) + 1;
	}
	printf("sum = %d\n", sum);
}

void Task15() // Тестом Миллера-Рабина, не забыть вырубить ограничение генератора случ чисел.
{
	int min, max;
	cout << "lower limit: ";
	cin >> min;
	cout << "upper limit: ";
	cin >> max;
	omp_set_num_threads(4);
#pragma omp parallel for
	for (int i = min; i <= max; i++) 
	{
		if (IsPrime(i))
			printf("%d\n", i);
	}
}

int main()
{
	Task15();
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
